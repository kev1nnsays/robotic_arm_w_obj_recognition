import cv2
import numpy as np
from PyQt4 import QtGui, QtCore, Qt

class Video():
    def __init__(self,capture):
        self.capture = capture
        self.capture.set(3, 1280)
        self.capture.set(4, 960)
        self.currentFrame=np.array([])
	self.cameraMatrix=0.0 
	self.dist=0.0
	self.new_camera_matrix=0.0
        self.CAL = False

	self.MIN_X = 310
	self.MAX_X = 950
	self.MIN_Y = 30
	self.MAX_Y = 510

	self.temp_pts=[]
	self.x=0.0
	self.y=0.0
	self.def_template_pt1 = False
	self.def_template_pt2 = False
	self.pressed = False

	""" 
        Affine Calibration Variables 
        Currently only takes three points: center of arm and two adjacent 
        corners of the base board
        Note that OpenCV requires float32 arrays
        """
        self.aff_npoints = 4                                     # Change!
        self.real_coord = np.float32([[-305., 305.], [-305.,-305.], [305.,-305.],[305.,305.]])
        self.mouse_coord = np.float32([[0.0, 0.0],[0.0, 0.0],[0.0, 0.0],[0.0,0.0]])      
        self.mouse_click_id = 0;
        self.aff_flag = 0;
        self.aff_matrix = np.float32((2,3))
    
    def captureNextFrame(self):
        """                      
        Capture frame, convert to RGB, and return opencv image      
        """
        ret, frame=self.capture.read()
        if(ret==True):
            self.currentFrame=cv2.cvtColor(frame, cv2.COLOR_BAYER_GB2BGR)
	    if self.CAL:
	    	self.currentFrame = cv2.undistort(self.currentFrame,self.cameraMatrix,self.dist,None,self.new_camera_matrix)
    
    def convertFrame(self):
        """ Converts frame to format suitable for QtGui  """
        try:
            height,width=self.currentFrame.shape[:2]
            img=QtGui.QImage(self.currentFrame,
                              width,
                              height,
                              QtGui.QImage.Format_RGB888)
            img=QtGui.QPixmap.fromImage(img)
            self.previousFrame = self.currentFrame

	    # These values are scaled down by a factor of 2 compared to original image
	    # Capturing template top left and bottom right points from the mouse clicks
	    if self.def_template_pt1 and self.pressed:
	        self.temp_pts.append([self.x,self.y])
	        self.def_template_pt1 = False
	        self.def_template_pt2 = True
		self.pressed = False
	    elif self.def_template_pt2 and self.pressed:
	        self.temp_pts.append([self.x,self.y])
                self.def_template_pt2 = False
		self.pressed = False
	   
            return img

		
        except:
            return None

    def loadCalibration(self):
        """
        Load calibration from file and applies to the image
        Look at camera_cal.py final lines to see how that is done
        """
	print "Load Calibration file"
	# Add check to ensure file exists else give an error

	self.cameraMatrix = np.load('CameraCalibration.npy')
	self.dist = np.load('DistortionCoeffs.npy')

	self.new_camera_matrix,roi = cv2.getOptimalNewCameraMatrix(self.cameraMatrix,self.dist,(1280,960),1,(1280,960))
	self.CAL = True
		
	
