import cv2
import numpy as np
from matplotlib import pyplot as plt

def temp_matcher(image,temp):
    # Loop over the size of original image and stop just before the width - width of template
    print"Matching Template.."
    img = np.asarray(image,dtype='int16')
    template = np.asarray(temp,dtype='int16')
    del_row = template.shape[::-1][0]
    del_col = template.shape[::-1][1]
    WID = img.shape[::-1][0] - del_row
    HTH = img.shape[::-1][1] - del_col
    op = np.zeros((WID,HTH))
    for row in range(img.shape[::-1][0] - del_row):
        for col in range(img.shape[::-1][1] - del_col):
            
            # Apply corresponding function to verify if matches (1. normalized SQDIFF 2.normalized cross correlation)
            num = np.sum(np.power(np.subtract(template, img[col:col+del_col,row:row+del_row]),2)) 
	    norm_denom = np.sqrt(np.sum(np.power(template,2))*np.sum(np.power(img[col:col+del_col,row:row+del_row],2)))	            
            op[row,col] = num/norm_denom
	    if np.isnan(op[row,col]):
		op[row,col] = 1.0
    return op	            
            

def suppress(temp_map,template,threshold):
    del_row = template.shape[::-1][0]
    del_col = template.shape[::-1][1]
    locmx = []
    loc = []
    temp_map = np.asarray(temp_map,dtype='float16')
    for row in np.arange(0,temp_map.shape[::-1][0],del_row):
        for col in np.arange(0,temp_map.shape[::-1][1],del_col):
	    mapp = temp_map[col:col+del_col,row:row+del_row]
            loc = np.where(mapp<=threshold) 
            if not len(loc[0])==0:
	    	#import pdb; pdb.set_trace()
            	locmx = np.where(mapp==np.max(mapp[loc]))
	    	max_val = np.max(mapp[loc])
		print max_val
		mapp[:,:]=1.0
		mapp[locmx] = max_val
            	mapp[locmx] = max_val
	    else:
		mapp[:,:] = 1.0
            temp_map[col:col+del_col,row:row+del_row] = mapp
    return temp_map 

def showImg(img):
    cv2.namedWindow('k',1)
    cv2.imshow('k',img)
    cv2.waitKey(10)
    cv2.waitKey(0)

if __name__=="__main__":
    img_color = cv2.imread('orig.jpg')
    #img_color = cv2.resize(img_color,(320,240))
    img = cv2.cvtColor(img_color,cv2.COLOR_BGR2GRAY)
    #img = cv2.resize(img,(320,240))
    template = cv2.imread('template.jpg',0)
    #template = img[73:93,145:168]
    w,h = template.shape[::-1]
    
    ## Obtain mapping scheme
    op = temp_matcher(img,template)
    print "Matching done"

    
    # Threshold mapping scheme to obtain matches
    threshold = 0.1
    # Optional suppression
    op = suppress(op,template,threshold)
    loc = np.where(np.transpose(op)<=threshold)
    for pt in zip(*loc[::-1]):
        # May possibly need suppression scheme to pick highest within a region
        cv2.rectangle(img_color,pt,(pt[0]+w,pt[1]+h),(0,0,255),2)
    cv2.imshow('k',img_color)
    cv2.waitKey(10)
    cv2.waitKey(0)
