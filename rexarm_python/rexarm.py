import lcm
import time
import sys
import numpy as np
from math import pi
from lcmtypes import dynamixel_command_t
from lcmtypes import dynamixel_command_list_t
from lcmtypes import dynamixel_status_t
from lcmtypes import dynamixel_status_list_t

from math import *

PI = np.pi
D2R = PI/180.0
ANGLE_TOL = 2*PI/180.0 


""" Rexarm Class """
class Rexarm():
    def __init__(self):

        """ Commanded Values """
        self.joint_angles = [0.0, 0.0, 0.0, 0.0] # radians
        self.speed_limit = 0.02
	self.speed = [0.5,0.5,0.5,0.5]           # 0 to 1, 0.05 optimal
        self.max_torque = 0.5                    # 0 to 1

        """ Feedback Values """
        self.joint_angles_fb = [0.0, 0.0, 0.0, 0.0] # radians
        self.speed_fb = [0.0, 0.0, 0.0, 0.0]        # 0 to 1   
        self.load_fb = [0.0, 0.0, 0.0, 0.0]         # -1 to 1  
        self.temp_fb = [0.0, 0.0, 0.0, 0.0]         # Celsius               

        """ Plan """
        self.plan = []
        self.plan_speed = []	# waypoint specific speed
	self.plan_status = 0
	self.begin_time = 0	
        self.wpt_number = 0	# Current waypoint number
        self.wpt_total = 0	# Total number of waypoints in the plan
	
        """ LCM Stuff"""
        self.lc = lcm.LCM()
        lcmMotorSub = self.lc.subscribe("ARM_STATUS",
                                        self.feedback_handler)

    def cmd_publish(self):
        """ 
        Publish the commands to the arm using LCM. 
        NO NEED TO CHANGE.
        You need to activelly call this function to command the arm.
        You can uncomment the print statement to check commanded values.
        """    
        msg = dynamixel_command_list_t()
        msg.len = 4
        self.clamp()
        for i in range(msg.len):
            cmd = dynamixel_command_t()
            cmd.utime = int(time.time() * 1e6)
            cmd.position_radians = self.joint_angles[i]
	    cmd.speed = self.speed[i]
            cmd.max_torque = self.max_torque
            msg.commands.append(cmd)
        self.lc.publish("ARM_COMMAND",msg.encode())
    
    def get_feedback(self):
        """
        LCM Handler function
        Called continuously from the GUI 
        NO NEED TO CHANGE
        """
        self.lc.handle_timeout(50)

    def feedback_handler(self, channel, data):
        """
        Feedback Handler for LCM
        NO NEED TO CHANGE FOR NOW.
        LATER NEED TO CHANGE TO MANAGE PLAYBACK FUNCTION 
        """
        msg = dynamixel_status_list_t.decode(data)
        for i in range(msg.len):
            self.joint_angles_fb[i] = msg.statuses[i].position_radians 
            self.speed_fb[i] = msg.statuses[i].speed 
            self.load_fb[i] = msg.statuses[i].load 
            self.temp_fb[i] = msg.statuses[i].temperature

    def clamp(self):
        """
        Clamp Function
        Limit the commanded joint angles to ones physically possible so the 
        arm is not damaged.
        """
	# Clamp function B: +-180, S,E,W: +-120 (limits in degrees)
	for i in range(4):
		if i==0:
			# Special clamp value for Base joint only
			if self.joint_angles[i]>PI:
				self.joint_angles[i]=PI
			elif self.joint_angles[i]<-PI:
				self.joint_angles[i]=-PI
		else:
			if self.joint_angles[i]>PI/3*2:
				self.joint_angles[i]=PI/3*2
			elif self.joint_angles[i]<-PI/3*2:
				self.joint_angles[i]=-PI/3*2
			

    def plan_command(self):
	divisionNumber = 51.0
	d = [69.26, 47.76, 99.98, 99.76, 109.26]
	
	# If condition to ensure that when current waypoint number exceeds the limit do not perform any activity
	if self.wpt_number < self.wpt_total:
        	self.max_torque = 0.40
		
		# Threshold value to ensure current position is reasonably close to desired waypoint
		if np.sum(abs(np.asarray(self.plan[self.wpt_number]) - np.asarray(self.joint_angles_fb))) < 0.1: 
	
			# Data capture functions
		        fopen_t = open('time_waypoints.csv','a')
			fopen = open('time_path.csv','a')
		        fopen_p = open('planned_path.csv','a')
		        fopen_a = open('actual_path.csv','a')
			fopen_p_s = open('planned_speed.csv','a')
			fopen_a_s = open('actual_speed.csv','a')
		        fopen_end_p = open('end_position_p.csv','a')
			fopen_end_a = open('end_position_a.csv','a')

			if self.wpt_number == 0:
				self.begin_time = time.time()	
				fopen.write("0.00\n")
				fopen_t.write("0.00\n")
			else:
				time_buf = time.time() - self.begin_time
				fopen.write("%.2f\n" % time_buf)
		
				if self.wpt_number%(3*divisionNumber)==divisionNumber:
					fopen_t.write("%.2f\n" % time_buf)

		        fopen_p.write("%f,%f,%f,%f\n" % (self.plan[self.wpt_number][0],self.plan[self.wpt_number][1],self.plan[self.wpt_number][2],self.plan[self.wpt_number][3]))
   			fopen_a.write("%f,%f,%f,%f\n" % (self.joint_angles_fb[0],self.joint_angles_fb[1],self.joint_angles_fb[2],self.joint_angles_fb[3]))

			end_buf = self.rexarm_FK(d,4,self.plan[self.wpt_number])
			fopen_end_p.write("%f,%f,%f,%f\n" % (end_buf[0],end_buf[1],end_buf[2],end_buf[3]))
	                end_buf = self.rexarm_FK(d,4,self.joint_angles_fb)
			fopen_end_a.write("%f,%f,%f,%f\n" % (end_buf[0],end_buf[1],end_buf[2],end_buf[3]))



			fopen_p_s.write("%f,%f,%f,%f\n" % (self.plan_speed[self.wpt_number][0],self.plan_speed[self.wpt_number][1],self.plan_speed[self.wpt_number][2],self.plan_speed[self.wpt_number][3]))
                        fopen_a_s.write("%f,%f,%f,%f\n" % (self.speed_fb[0],self.speed_fb[1],self.speed_fb[2],self.speed_fb[3]))
       			fopen_end_p.close()
			fopen_end_a.close()
			fopen_t.close()
			fopen.close()
		        fopen_p.close()
		        fopen_a.close()
			fopen_p_s.close()
			fopen_a_s.close()
	
			self.wpt_number += 1

			# Saturate the possible value of current updated waypoint number
			if self.wpt_number >= self.wpt_total:
				self.wpt_number= self.wpt_total

		# Publish the desired waypoint value as joint angles and desired speed	
		if self.wpt_number < self.wpt_total:
			self.joint_angles = self.plan[self.wpt_number]
			self.speed = self.plan_speed[self.wpt_number]
		
			self.cmd_publish()
	

    def rexarm_FK(self,dh_table, linki,theta):
        """
        Calculates forward kinematics for rexarm
        takes a DH table filled with DH parameters of the arm
        and the link to return the position for
        returns a 4-tuple (x, y, z, phi) representing the pose of the 
        desired link
        """
	# Acquire dh table
	d = dh_table
	
	T = list()
	
	# If condition allows for use of the rexarm_FK function with desired input joint angles or using feedback
	if len(theta)==0:
		t = [0, self.joint_angles_fb[0], self.joint_angles_fb[1]+pi/2.0,self.joint_angles_fb[2],self.joint_angles_fb[3]]
	else:
		t = [0, theta[0], theta[1]+pi/2.0,theta[2],theta[3]]
	
	# DH parameters	
	alpha = [0, pi/2.0,0,0,0]
	a = [0,0,d[2],d[3], d[4]]
	da = [d[0], d[1], 0,0,0]

	# Generate required matrices based on number of links (currently set to static value for end effector. Please change to obtain result for desired link)
	for i in range(5):
		T.append( np.asarray([
		[cos(t[i]),-sin(t[i])*cos(alpha[i]), sin(t[i])*sin(alpha[i]),a[i]*cos(t[i])],
		[sin(t[i]),cos(t[i])*cos(alpha[i]),-cos(t[i])*sin(alpha[i]),a[i]*sin(t[i])],
		[0,sin(alpha[i]),cos(alpha[i]),da[i]],
		[0,0,0,1]]) )
	
	# Multiplying the desired matrices	
	finalT = T[0]
	for i in range(len(T)-1):
		finalT = np.dot(finalT,T[i+1])

	# Method 2 simple geometrical solution	
	#X = (d2*sin(t[1]) + d3*sin(t[2]+t[1]) + d4*sin(t[3]+t[2]+t[1]))*sin(-t[0])
	#Y = ((d2*sin(t[1])) + d3*sin(t[2]+t[1]) + d4*sin(t[3]+t[2]+t[1]))*cos(-t[0])
	#Z = d0 + d1 + d2*cos(t[1]) + d4*cos(t[3]+t[2]+t[1]) + d3*cos(t[2]+t[1])


	# Calculation of attack angle phi (with threshold)
	phi = ((np.sum(np.asarray(self.joint_angles_fb[1:4]))*180.0/np.pi )+ 90.0)
	if phi > 90.0:
		phi =  180.0 - phi
	
	# Return final values from calculated matrix
	[X,Y,Z,PHI] = [finalT[0][3],finalT[1][3],finalT[2][3],phi]
    	return ([X,Y,Z,PHI])
	
    def rexarm_IK(self,pose, d,cfg):
        """
        Calculates inverse kinematics for the rexarm
        pose is a tuple (x, y, z, phi) which describes the desired
        end effector position and orientation.  
        cfg describe elbow down (0) or elbow up (1) configuration
        returns a 4-tuple of joint angles or NONE if configuration is impossible
        """
        theta = [0.0,0.0,0.0,0.0]
        [x,y,z,phi] = pose
        L = np.sqrt(x**2+y**2+(z-d[0]-d[1])**2)
         
	if L< (d[2]+d[3]+d[4]):
		theta[0] = np.arctan2(y,x) 
        
		theta[2] = acos( (d[4]**2 - d[2]**2 - (L-d[2])**2) / (2*d[2]*(d[3]-L)) ) 
        	theta[3] = asin( (d[2]/d[4])*sin(theta[2])  ) 
        	theta[1] = np.pi/2 - theta[1] - theta[2] - asin( (z-d[1]-d[0])/L )

        	theta[1] = -1*theta[1]
        	theta[2] = -1*theta[2]
        	theta[3] = -1*theta[3]	
		
	# Degenerate cases where workspace point desired is not reachable
	if L > (d[2]+d[3]+d[4]):
		#theta = old_theta
		print "Unreachable position, retaining default position"
		return [0.0,0.0,0.0,0.0],False
	elif theta[0]>PI or  theta[0]<-PI:
		print "Unreachable position, retaining default position"
		return theta[0:4],False
	elif theta[1]>PI/3*2 or theta[1]<-PI/3*2:
		print "Unreachable position, retaining default position"
		return theta[0:4],False
	elif theta[2]>PI/3*2 or theta[2]<-PI/3*2:
		print "Unreachable position, retaining default position"
		return theta[0:4],False
	elif theta[3]>PI/3*2 or theta[3]<-PI/3*2:
		print "Unreachable position, retaining default position"
		return theta[0:4],False
	else:		
 		return theta[0:4],True


    def rexarm_collision_check(q):
        """
        Perform a collision check with the ground and the base
        takes a 4-tuple of joint angles q
        returns true if no collision occurs
        """
        pass 
