import sys
import cv2
import numpy as np
from PyQt4 import QtGui, QtCore, Qt
from ui import Ui_MainWindow
from rexarm import Rexarm
import time
from video import Video
import copy
import matplotlib.pyplot as plt
import cv2
from numpy import genfromtxt

""" Radians to/from  Degrees conversions """
D2R = 3.141592/180.0
R2D = 180.0/3.141592

""" Pyxel Positions of image in GUI """
MIN_X = 310
MAX_X = 950

MIN_Y = 30
MAX_Y = 510
#asdf 
class Gui(QtGui.QMainWindow):
    """ 
    Main GUI Class
    It contains the main function and interfaces between 
    the GUI and functions
    """
    def __init__(self,parent=None):
        QtGui.QWidget.__init__(self,parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

	# Initialization of required variables
	self.dhlist = list()
        self.waypoints = list()
	self.waypoints_final = list()
	self.waypoints_speed_final = list()
	self.a = [[0.0,0.0,0.0,0.0],[0.0,0.0,0.0,0.0],[0.0,0.0,0.0,0.0],[0.0,0.0,0.0,0.0]]
	self.centers = []
	self.timer0_t = 60.0
	self.splinePts = 200.0
	self.cubDt = 2.0
	self.d = [69.26, 47.76, 99.98, 99.76, 109.26]
	self.TEMP_MATCH = False # Flag to ensure template matching is complete
	self.mousecoords = np.float32([0.0,0.0])
	""" Main Variables Using Other Classes"""
        self.rex = Rexarm()
        self.video = Video(cv2.VideoCapture(0))

        """ Other Variables """
        self.last_click = np.float32([0,0])

        """ Set GUI to track mouse """
        QtGui.QWidget.setMouseTracking(self,True)

        """ 
        Video Function 
        Creates a timer and calls play() function 
        according to the given time delay (27mm) 
        """
        self._timer = QtCore.QTimer(self)
        self._timer.timeout.connect(self.play)
        self._timer.start(27)
       
        """ 
        LCM Arm Feedback
        Creates a timer to call LCM handler continuously
        No delay implemented. Reads all time 
        """  
        self._timer2 = QtCore.QTimer(self)
        self._timer2.timeout.connect(self.rex.get_feedback)
        self._timer2.start()

        """ 
        Connect Sliders to Function
        TO DO: CONNECT THE OTHER 5 SLIDERS IMPLEMENTED IN THE GUI 
        """ 
        self.ui.sldrBase.valueChanged.connect(self.slider_change)
        self.ui.sldrShoulder.valueChanged.connect(self.slider_change)
        self.ui.sldrElbow.valueChanged.connect(self.slider_change)
        self.ui.sldrWrist.valueChanged.connect(self.slider_change)

	self.ui.sldrMaxTorque.valueChanged.connect(self.slider_change)
	self.ui.sldrSpeed.valueChanged.connect(self.slider_change)

        """ Commands the arm as the arm initialize to 0,0,0,0 angles """
        self.slider_change() 
        
        """ Connect Buttons to Functions """
        self.ui.btnLoadCameraCal.clicked.connect(self.load_camera_cal)
        self.ui.btnPerfAffineCal.clicked.connect(self.affine_cal)
        self.ui.btnTeachRepeat.clicked.connect(self.tr_initialize)
        self.ui.btnAddWaypoint.clicked.connect(self.tr_add_waypoint)
        self.ui.btnSmoothPath.clicked.connect(self.tr_smooth_path)
        self.ui.btnPlayback.clicked.connect(self.tr_playback)
        self.ui.btnDefineTemplate.clicked.connect(self.def_template)
        self.ui.btnLocateTargets.clicked.connect(self.template_match)
        self.ui.btnExecutePath.clicked.connect(self.exec_path)
	self.ui.btnLoadPlan.clicked.connect(self.load_path)	


    def play(self):
        """ 
        Play Funtion
        Continuously called by GUI 
        """

        """ Renders the Video Frame """
        try:
            self.video.captureNextFrame()
            self.ui.videoFrame.setPixmap(
                self.video.convertFrame())
            self.ui.videoFrame.setScaledContents(True)
        except TypeError:
            print "No frame"
        
        """ 
        Update GUI Joint Coordinates Labels
        TO DO: include the other slider labels 
        """
        self.ui.rdoutBaseJC.setText(str(self.rex.joint_angles_fb[0]*R2D))
        self.ui.rdoutShoulderJC.setText(str(self.rex.joint_angles_fb[1]*R2D))
        self.ui.rdoutElbowJC.setText(str(self.rex.joint_angles_fb[2]*R2D))
        self.ui.rdoutWristJC.setText(str(self.rex.joint_angles_fb[3]*R2D))
	
	
	# Generate DHlist and pass to rexarm_FK (to update world coordinates)
	[X,Y,Z,phi] = self.rex.rexarm_FK(self.d,4,[])
	self.ui.rdoutX.setText("%.2f" %X)
	self.ui.rdoutY.setText("%.2f" %Y)
	self.ui.rdoutZ.setText("%.2f" %Z)
        self.ui.rdoutT.setText("%.2f" %phi)

	""" 
        Mouse position presentation in GUI
        TO DO: after getting affine calibration make the apprriate label
        to present the value of mouse position in world coordinates 
        """    
        x = QtGui.QWidget.mapFromGlobal(self,QtGui.QCursor.pos()).x()
        y = QtGui.QWidget.mapFromGlobal(self,QtGui.QCursor.pos()).y()
        if ((x < MIN_X) or (x > MAX_X) or (y < MIN_Y) or (y > MAX_Y)):
            self.ui.rdoutMousePixels.setText("(-,-)")
            self.ui.rdoutMouseWorld.setText("(-,-)")
        else:
            x = x - MIN_X
            y = y - MIN_Y
            self.ui.rdoutMousePixels.setText("(%.0f,%.0f)" % (x,y))
            if (self.video.aff_flag == 2):
                """ TO DO Here is where affine calibration must be used """
		[self.mousecoords[0],self.mousecoords[1],dummy] = np.dot(self.video.aff_matrix,np.asarray([x,y,1]))
                self.ui.rdoutMouseWorld.setText("(%.0f,%.0f)" %(self.mousecoords[0],self.mousecoords[1]))
            else:
                self.ui.rdoutMouseWorld.setText("(-,-)")

        """ 
        Updates status label when rexarm playback is been executed.
        This will be extended to includ eother appropriate messages
        """ 
        if(self.rex.plan_status == 1):
            self.ui.rdoutStatus.setText("Playing Back - Waypoint %d"
                                    %(self.rex.wpt_number + 1))


    def slider_change(self):
        """ 
        Function to change the slider labels when sliders are moved
        and to command the arm to the given position 
        TO DO: Implement for the other sliders
        """
        self.ui.rdoutBase.setText(str(self.ui.sldrBase.value()))
        self.ui.rdoutShoulder.setText(str(self.ui.sldrShoulder.value()))
        self.ui.rdoutElbow.setText(str(self.ui.sldrElbow.value()))
        self.ui.rdoutWrist.setText(str(self.ui.sldrWrist.value()))
        
	self.ui.rdoutTorq.setText(str(self.ui.sldrMaxTorque.value()) + "%")
        self.rex.max_torque = self.ui.sldrMaxTorque.value()/100.0
        
	self.rex.joint_angles[0] = self.ui.sldrBase.value()*D2R
	self.rex.joint_angles[1] = self.ui.sldrShoulder.value()*D2R
	self.rex.joint_angles[2] = self.ui.sldrElbow.value()*D2R
	self.rex.joint_angles[3] = self.ui.sldrWrist.value()*D2R

	self.ui.rdoutSpeed.setText(str(self.ui.sldrSpeed.value()))

	self.rex.cmd_publish()

    def mousePressEvent(self, QMouseEvent):
        """ 
        Function used to record mouse click positions for 
        affine calibration 
        """
 
        """ Get mouse posiiton """
        x = QMouseEvent.x()
        y = QMouseEvent.y()
	self.video.pressed = True

        """ If mouse position is not over the camera image ignore """
        if ((x < MIN_X) or (x > MAX_X) or (y < MIN_Y) or (y > MAX_Y)): return

        """ Change coordinates to image axis """
        self.last_click[0] = x - MIN_X
        self.last_click[1] = y - MIN_Y
       
        """ If affine calibration is been performed """
        if (self.video.aff_flag == 1):
            """ Save last mouse coordinate """
            self.video.mouse_coord[self.video.mouse_click_id] = [(x-MIN_X),
                                                                 (y-MIN_Y)]

            """ Update the number of used poitns for calibration """
            self.video.mouse_click_id += 1

            """ Update status label text """
            self.ui.rdoutStatus.setText("Affine Calibration: Click point %d" 
                                      %(self.video.mouse_click_id + 1))

            """ 
            If the number of click is equal to the expected number of points
            computes the affine calibration.
            TO DO: Change this code to use you programmed affine calibration
            and NOT openCV pre-programmed function as it is done now.
            """
            if(self.video.mouse_click_id == self.video.aff_npoints):
                """ 
                Update status of calibration flag and number of mouse
                clicks
                """
                self.video.aff_flag = 2
                self.video.mouse_click_id = 0
                
                """ Perform affine calibration without OpenCV """
		
		mouse_coord = self.video.mouse_coord.tolist()
		real_coord = self.video.real_coord.tolist()

		# Formulation of system matrices A and b as shown in report 
                A = [[mouse_coord[0][0],mouse_coord[0][1],1,0,0,0],[0,0,0,mouse_coord[0][0],mouse_coord[0][1],1],[mouse_coord[1][0],mouse_coord[1][1],1,0,0,0],[0,0,0,mouse_coord[1][0],mouse_coord[1][1],1],[mouse_coord[2][0],mouse_coord[2][1],1,0,0,0],[0,0,0,mouse_coord[2][0],mouse_coord[2][1],1],[mouse_coord[3][0],mouse_coord[3][1],1,0,0,0],[0,0,0,mouse_coord[3][0],mouse_coord[3][1],1]]
                b = [real_coord[0][0],real_coord[0][1],real_coord[1][0],real_coord[1][1],real_coord[2][0],real_coord[2][1],real_coord[3][0],real_coord[3][1]]
		
		A = np.asarray(A)
		b=np.asarray(b)
		
		# Solving the pseudo inverse problem to identify the affine matrix
		self.video.aff_matrix = np.dot(np.linalg.pinv(A),b)
		self.video.aff_matrix = np.hstack((self.video.aff_matrix,np.array([0,0,1])))
		self.video.aff_matrix = np.reshape(self.video.aff_matrix,(3,3))            
		
		""" Updates Status Label to inform calibration is done """ 
                self.ui.rdoutStatus.setText("Waiting for input")

	# Set values from pixel coordinates for defining the template 
	if self.video.def_template_pt1 or self.video.def_template_pt2:
		[self.video.x,self.video.y] = [(x-MIN_X),(y-MIN_Y)] 

    def affine_cal(self):
        """ 
        Function called when affine calibration button is called.
	 the flag to record the next mouse clicks
        and updates the status text label 
        """
        self.video.aff_flag = 1 
        self.ui.rdoutStatus.setText("Affine Calibration: Click point %d" 
                                    %(self.video.mouse_click_id + 1))

    def load_camera_cal(self):
        print "Load Camera Cal"
	self.video.loadCalibration()

    def tr_initialize(self):
        print "Teach and Repeat"
	
	# Reset our lists
	self.rex.plan = list()
	self.rex.plan_speed = list()
	self.waypoints_final = list()
	self.waypoints_speed_final = list()

	self.rex.max_torque = 0.0

	# Set base velocity
	self.rex.speed_b = 0.02

	self.rex.cmd_publish()

    def tr_add_waypoint(self):
        print "Add Waypoint"

	# If case to check when template matching is done. If so calculate specific waypoints using IK and append to the plan
	if self.TEMP_MATCH:
		for iter in self.centers:
            		[tempy,tempx,dummy2] = np.dot(self.video.aff_matrix,np.asarray([iter[0],iter[1],1]))
            		temp = self.rex.rexarm_IK([-tempx,tempy,0,-90*np.pi/180],self.d,0) # Ensures attack angle phi is -90 for safest approach
            		self.waypoints_final.append(temp)
            		self.waypoints_speed_final.append([self.rex.speed_b,self.rex.speed_b,self.rex.speed_b,self.rex.speed_b])

	# If no template matching is performed add standard joint angles from feedback
	else:
		self.rex.plan.append([self.rex.joint_angles_fb[0],self.rex.joint_angles_fb[1],self.rex.joint_angles_fb[2],self.rex.joint_angles_fb[3]])
		self.rex.plan_speed.append([self.rex.speed_b,self.rex.speed_b,self.rex.speed_b,self.rex.speed_b])	

    # This function is used to avoid the problem of shallow copy issue while appending to the self.rex.plan variable
    def realAppend(self,newPoints):
	self.rex.plan = copy.deepcopy(self.rex.plan)	
	self.rex.plan.append(newPoints)
	self.rex.plan = copy.deepcopy(self.rex.plan)
	 
	
    def load_path(self):
	if self.rex.plan:
		fopen = open('waypoint_path.csv','w')
		for item in self.rex.plan:
			fopen.write("%f,%f,%f,%f\n" % (item[0],item[1],item[2],item[3]))
		fopen.close()
		fopen = open('waypoint_speed.csv','w')
		for item in self.rex.plan_speed:
		#	print item
			fopen.write("%f,%f,%f,%f\n" % (item[0],item[1],item[2],item[3]))
		fopen.close()
                fopen = open('end_position_w.csv','w')
                for it in self.rex.plan:
                #       print item
			item = self.rex.rexarm_FK(self.d,4,it)
                        fopen.write("%f,%f,%f,%f\n" % (item[0],item[1],item[2],item[3]))
                fopen.close()
		
	else:
		fopen = open('waypoint_path.csv','r')
		self.rex.plan = genfromtxt(fopen,delimiter = ',')
		fopen.close()
		fopen = open('waypoint_speed.csv','r')
		self.rex.plan_speed = genfromtxt(fopen,delimiter = ',')
		fopen.close()
		
		#For section 3.1, set different speed
		for i in range(len(self.rex.plan_speed)):
			self.rex.plan_speed[i] = [0.02,0.02,0.02,0.02]	

    def tr_smooth_path(self):
        print "Smooth Path"
	
	#Add safety points before and after each waypoint
	self.addSafetyPts()
	
	# Cubic inteprolation
	self.cubIntp()
	
	# Linear interpolation
	#sel.linIntp()


    #Add points before and after waypoint   
    def addSafetyPts(self):
	print "Adding safety points"
	
	# If template match and affine calibration is done introduce specific safety points based on IK (before and after the waypoint)
	if self.TEMP_MATCH and self.video.aff_flag==2:
		for iter in self.centers:
            		[tempx,tempy,dummy2] = np.dot(self.video.aff_matrix,np.asarray([iter[0],iter[1],1]))
			print tempx,tempy
	    		temp,flag = self.rex.rexarm_IK([tempx,tempy,50,-90*np.pi/180],self.d,0)
	    		if flag:
				self.waypoints_final.append([temp[0],temp[1],temp[2],temp[3]])
            			self.waypoints_speed_final.append([self.rex.speed_b,self.rex.speed_b,self.rex.speed_b,self.rex.speed_b])

	    			temp1,flag = self.rex.rexarm_IK([tempx,tempy,0,-90*np.pi/180],self.d,0)
            			self.waypoints_final.append([temp1[0],temp1[1],temp1[2],temp1[3]])            
            			self.waypoints_speed_final.append([self.rex.speed_b,self.rex.speed_b,self.rex.speed_b,self.rex.speed_b])


	    			self.waypoints_final.append([temp[0],temp[1],temp[2],temp[3]])
            			self.waypoints_speed_final.append([self.rex.speed_b,self.rex.speed_b,self.rex.speed_b,self.rex.speed_b])

       			else: 
				print "Removed point due to unreachable position."
	else:

	########## for teach and repeat!!!##########
	      for iter in self.rex.plan:
	      # 3.2 Virtual safety points before desired waypoint
	       if iter[1] + 20.0*D2R > 0.0:
	       	temp = 0.0*D2R
	       else:
	       	temp = iter[1] + 20.0*D2R
	       
	       self.waypoints_final.append([iter[0],temp,iter[2],iter[3]])
	       self.waypoints_speed_final.append([self.rex.speed_b,self.rex.speed_b,self.rex.speed_b,self.rex.speed_b])
	       self.waypoints_final.append(iter)
	       self.waypoints_speed_final.append([self.rex.speed_b,self.rex.speed_b,self.rex.speed_b,self.rex.speed_b])
	       self.waypoints_final.append([iter[0],temp,iter[2],iter[3]])
	       self.waypoints_speed_final.append([self.rex.speed_b,self.rex.speed_b,self.rex.speed_b,self.rex.speed_b])
	
	self.rex.plan =  self.waypoints_final
	self.rex.plan_speed = self.waypoints_speed_final
    
    #Function for cubic interpolation
    def cubIntp(self):
	print "Generating cubic interpolated waypoints"
	self.waypoints_final = list()
	self.waypoints_speed_final = list()
	joint_angles = [0.0,0.0,0.0,0.0]
	speed = [0.0,0.0,0.0,0.0]
	for main_idx in range(len(self.rex.plan)):
		if main_idx!=0:
			self.cubInpCoeff(self.rex.plan[main_idx-1][:],self.rex.plan[main_idx][:]) #find the coefficients
			self.waypoints_final.append([self.rex.plan[main_idx-1][0],self.rex.plan[main_idx-1][1],self.rex.plan[main_idx-1][2],self.rex.plan[main_idx-1][3]])
			self.waypoints_speed_final.append([self.rex.speed_b,self.rex.speed_b,self.rex.speed_b,self.rex.speed_b])	
			ttime = 0
			self.splinePts = 51 
			self.timer0_t = self.cubDt*1000/self.splinePts

			for main_iter in range(self.splinePts):
				for idx in range(4):
					joint_angles[idx] = (self.a[idx][0]) + (self.a[idx][1]*ttime) + (self.a[idx][2]*pow(ttime,2)) + (self.a[idx][3]*pow(ttime,3))
                                	speed[idx] = abs((self.a[idx][1]) + (2*self.a[idx][2]*ttime) + (3*self.a[idx][3]*pow(ttime,2))) 
				
				ttime = ttime + self.timer0_t/1000.0
				self.waypoints_final.append([joint_angles[0],joint_angles[1],joint_angles[2],joint_angles[3]])
				self.waypoints_speed_final.append([speed[0],speed[1],speed[2],speed[3]])
        
	self.rex.plan =  self.waypoints_final
        self.rex.plan_speed = self.waypoints_speed_final

    #Function to find coefficients for cubic interpolation
    def cubInpCoeff(self,qt,qft):
       	self.strtTime = time.time()
       	for idx in range(4):
               	q = qt[idx]
               	qf = qft[idx]
               	v = 0.05
               	vf = 0.05
                dt = 1.0 
		self.cubDt = dt
       	        self.a[idx][0] = q
               	self.a[idx][1] = v
                self.a[idx][2] = ((3*(qf-q))-((2*v+vf)*(dt)))/pow(dt,2)
       	        self.a[idx][3] = ((2*(q-qf))+((v+vf)*(dt)))/pow(dt,3)

    #Function to linearly interpolate a given set of waypoints
    def linIntp(self):
	print "Linearly interpolating waypoints"
	# Need to modify and verify once again
	self.waypoints_f = list()
	self.waypoints_final = list()
	self.waypoints_speed_final = list()
	divisionNumber = 10.0 # must be odd
	count = 1

	for iter in self.rex.plan:
		if count ==1:
			temp = iter
			count = count + 1
		else:
			res = (np.asarray(iter) - np.asarray(temp))/(divisionNumber-1)
			
			max_r = max([abs(iter[0] - temp[0]),abs(iter[1]-temp[1]),abs(iter[2]-temp[2]),abs(iter[3]-temp[3])])
			r0 = abs(iter[0]-temp[0])
			r1 = abs(iter[1]-temp[1])
			r2 = abs(iter[2]-temp[2])
			r3 = abs(iter[3]-temp[3])			

			for num in range(int(divisionNumber)):
				self.waypoints_f.append(np.ndarray.tolist(np.asarray(temp) + res*(num)))
	       			
				if num > 0:
					self.waypoints_speed_final.append([max(self.rex.speed_b*r0/max_r,self.rex.speed_b/4),max(self.rex.speed_b*r1/max_r,self.rex.speed_b/4),max(self.rex.speed_b*r2/max_r,self.rex.speed_b/4),max(self.rex.speed_b*r3/max_r,self.rex.speed_b/4)])
				#self.waypoints_speed_final.append([0.5,0.5,0.5,0.5])
			self.waypoints_speed_final.append([self.rex.speed_b/4,self.rex.speed_b/4,self.rex.speed_b/4,self.rex.speed_b/4])
			temp = iter
                        count += 1

	# Simple moving average filter
	#temp_waypoints = np.transpose(self.waypoints_f)
	#temp_list = list()
	#for iter in temp_waypoints:
	#	temp_list.append(np.convolve(iter,[1.0/10,1.0/10,1.0/10],'same'))
	#final = np.transpose(temp_list).tolist()
	##print final
	
	#Reinstall the points back
	for i in range(len(self.waypoints_f)):
		if i%(divisionNumber-1)==0:
			self.waypoints_final.append(self.waypoints_f[int(i/(divisionNumber-1))]) 
		else:
			self.waypoints_final.append(self.waypoints_f[i])
	
	self.rex.plan = self.waypoints_f
	self.rex.plan_speed = self.waypoints_speed_final


	# Low pass filter using blackman window 	
	#w = np.blackman(15)
	#h = np.sinc(2*0.5*(np.arange(15)-(15-1)/2.))
	#h= h*w
	#h=h/np.sum(h)

	#B = np.convolve(self.waypoints_final[:][0],h,'same')
	#S = np.convolve(self.waypoints_final[:][1],h,'same')
	#E = np.convolve(self.waypoints_final[:][2],h,'same')
	#W = np.convolve(self.waypoints_final[:][3],h,'same')

	#self.waypoints_final[:][0] = B
	#self.waypoints_final[:][1] = S
	#self.waypoints_final[:][2] = E
	#self.waypoints_final[:][3] = W
					

    def tr_playback(self):
        print "Playback"
        #first clear the output file
	fopen_t = open('time_waypoints.csv','w')
	fopen = open('time_path.csv','w')
	fopen_p = open('planned_path.csv','w')
	fopen_a = open('actual_path.csv','w')
	fopen_p_s = open('planned_speed.csv','w')
	fopen_a_s = open('actual_speed.csv','w')
	fopen_end_p = open('end_position_p.csv','w')
	fopen_end_a = open('end_position_a.csv','w')
        #fopen.write("%f\n" % (time.time()))
	#fopen_p.write("%f,%f,%f,%f\n" % (self.rex.plan[0][0],self.rex.plan[0][1],self.rex.plan[0][2],self.rex.plan[0][3]))
	#fopen_a.write("%f,%f,%f,%f\n" % (self.rex.joint_angles_fb[0],self.rex.joint_angles_fb[1],self.rex.joint_angles_fb[2],self.rex.joint_angles_fb[3]))
	fopen.close()
	fopen_p.close()
	fopen_a.close()
	fopen_p_s.close()
	fopen_a_s.close()
	fopen_end_p.close()
	fopen_end_a.close()

	""" Creates a time and called tr_playback function
	every 20 milliseconds
	"""
	self.rex.wpt_number = 0
	self.rex.wpt_total = len(self.rex.plan)
        self._timer0 = QtCore.QTimer(self)
        self._timer0.timeout.connect(self.timePlay)
        self._timer0.start(self.timer0_t)
    
    # Plan execution using timer
    def timePlay(self):
	if self.rex.wpt_number == self.rex.wpt_total:
		self._timer0.stop()
	else:
		self.rex.plan_command()
	 
    def def_template(self):
        print "Define Template"
	self.video.def_template_pt1 = True
	

    def template_match(self):
        print "Template Match"
	img_color = self.video.currentFrame 
	img = cv2.cvtColor(img_color,cv2.COLOR_BGR2GRAY)
	orig_h,orig_w = img.shape[::-1]
	img = cv2.resize(img,(orig_h/2,orig_w/2))
	template = img[self.video.temp_pts[0][1]:self.video.temp_pts[1][1],self.video.temp_pts[0][0]:self.video.temp_pts[1][0]]	
	cv2.imwrite('template.jpg',template)
	cv2.imwrite('orig.jpg',img)
	w,h = template.shape[::-1]

	## Obtain mapping scheme
	start = time.time()
    	op = self.matcher(img,template)
    	print "Matching done"
	print time.time()-start

    	# Threshold mapping scheme to obtain matches
    	threshold = 0.1
    	# Optional suppression
    	op = self.suppress(op,template,threshold)
    	loc = np.where(np.transpose(op)<=threshold)
    	for pt in zip(*loc[::-1]):
        	# May possibly need suppression scheme to pick highest within a region
		# Return center of bounding boxes
		tx = 0.5*(2*pt[0]+w)
		ty = 0.5*(2*pt[1]+h)
		
		# Remove centers which are too close
                if not len(self.centers)==0:
			dist = np.linalg.norm((np.asarray(self.centers)-[tx,ty]),axis=1)
			idx = np.where(dist<=10.0)
			if len(idx[0])==0:
				self.centers.append([tx,ty])
				cv2.rectangle(img_color,(2*pt[0],2*pt[1]),(2*(pt[0]+w),2*(pt[1]+h)),(0,0,255),2)
		else:
			self.centers.append([tx,ty])
			cv2.rectangle(img_color,(2*pt[0],2*pt[1]),(2*(pt[0]+w),2*(pt[1]+h)),(0,0,255),2)
			
	print "Number of objects detected is: %d" %(len(self.centers))
	
	self.TEMP_MATCH = True
	cv2.imwrite('Final.jpg',img_color)
	import Image; image = Image.open('Final.jpg'); image.show()

    def suppress(self,temp_map,template,threshold):
	del_row = template.shape[::-1][0]
    	del_col = template.shape[::-1][1]
    	locmx = []
    	loc = []
    	temp_map = np.asarray(temp_map,dtype='float16')
    	for row in np.arange(0,temp_map.shape[::-1][0],del_row):
        	for col in np.arange(0,temp_map.shape[::-1][1],del_col):
            		mapp = temp_map[col:col+del_col,row:row+del_row]
            		loc = np.where(mapp<=threshold)                                                
            		if not len(loc[0])==0:
                	#import pdb; pdb.set_trace()
                		locmx = np.where(mapp==np.max(mapp[loc]))
                		max_val = np.max(mapp[loc])
               			mapp[:,:]=1.0
                		mapp[locmx] = max_val
                		mapp[locmx] = max_val
            		else:
                		mapp[:,:] = 1.0
            		temp_map[col:col+del_col,row:row+del_row] = mapp
    	return temp_map 

	
    def matcher(self,image,temp):
	print"Matching Template.."
    	img = np.asarray(image,dtype='int16')
    	template = np.asarray(temp,dtype='int16')
    	del_row = template.shape[::-1][0]
    	del_col = template.shape[::-1][1]
    	WID = img.shape[::-1][0] - del_row
    	HTH = img.shape[::-1][1] - del_col
    	op = np.zeros((WID,HTH))
    	
	for row in range(img.shape[::-1][0] - del_row):
        	for col in range(img.shape[::-1][1] - del_col):
            		# Apply corresponding function to verify if matches (1. normalized SQDIFF 2.normalized cross correlation)
            		num = np.sum(np.power(np.subtract(template, img[col:col+del_col,row:row+del_row]),2))
            		norm_denom = np.sqrt(np.sum(np.power(template,2))*np.sum(np.power(img[col:col+del_col,row:row+del_row],2)))
            		op[row,col] = num/norm_denom
        
	return op

    def exec_path(self):

 	
#	IKfile = open('IK_data.csv','w')
#	IKfile.close()
#	self.generateAndExec_IK_path(self.d)
	u,temp = self.rex.rexarm_IK([149.0,177.0, 0, -50*np.pi/180], self.d, 0)	
      	print u
	self.realAppend(u)
	self.rex.plan_speed.append([0.2,0.2,0.2,0.2])
	print "Execute fun Path"
	self.tr_playback()
    
    # Generation of fun path using equations	
    def generateAndExec_IK_path(self, d):
	print "Generating  and executing the path"
	start_t = time.time()
 	t = 0
	theta = 0.0
	
        IKfile = open('IK_w.csv','w')
        IKfile2 = open('IK_p_angle.csv','w')

	while theta < np.pi:
		wavelength = 1  
		k = (2*np.pi)/wavelength
		z = 60*np.sin(t*k) + 140
		r = 250
		x = r*np.cos(theta)
		y = r*np.sin(theta) 
		phi = -20
		 
		wp,flag = self.rex.rexarm_IK([x,y,z, phi*np.pi/180], d, 0)
		IKfile.write("%.2f,%.2f,%.2f\n" % (x,y,z))
		IKfile2.write("%.2f,%.2f,%.2f,%.2f\n" % (wp[0],wp[1],wp[2],wp[3]))
		
		if flag:
			self.rex.plan_speed.append([0.2,0.2,0.2,0.2])
			self.realAppend(wp)
		#time.sleep(0.10)
		t = t+0.01
		theta = theta + 1.0*np.pi/180
	IKfile.close()
	IKfile2.close()

	
	
def main():
    app = QtGui.QApplication(sys.argv)
    ex = Gui()
    ex.show()
    sys.exit(app.exec_())
 
if __name__ == '__main__':
    main()
