# Overview #
Implemented 4DOF arm that can:

1. Repeat a path that is taught by a user

2. Recognize objects using computer vision (template matching) and plan paths to the objects

Our implementation uses DH parameters for forward kinematics and constrained triangles for inverse kinematics. 

# Video Results #
https://www.youtube.com/playlist?list=PLo79OivWOV6dLDnN67etJu3fRQzESMVIP